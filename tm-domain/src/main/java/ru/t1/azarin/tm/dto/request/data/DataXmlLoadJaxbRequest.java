package ru.t1.azarin.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;

public final class DataXmlLoadJaxbRequest extends AbstractUserRequest {

    public DataXmlLoadJaxbRequest(@Nullable final String token) {
        super(token);
    }

}
